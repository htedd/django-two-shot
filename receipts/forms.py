from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class CreateReceipt(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )

    def __init__(self, *args, user=None, **kwargs):
        super(CreateReceipt, self).__init__(*args, **kwargs)
        if user is not None:
            self.fields["category"].queryset = ExpenseCategory.objects.filter(
                owner=user
            )
            self.fields["account"].queryset = Account.objects.filter(
                owner=user
            )


class CreateExpenseCategory(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class CreateAccount(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
