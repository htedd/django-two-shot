from django.shortcuts import render, redirect
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, CreateExpenseCategory, CreateAccount

# Create your views here.


@login_required
def home(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            create = form.save(False)
            create.purchaser = request.user
            create.save()
            return redirect("home")
    else:
        form = CreateReceipt(user=request.user)
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def edit_receipt(request, id):
    model_instance = Receipt.objects.get(id=id)
    if request.method == "POST":
        form = CreateReceipt(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("home")
    else:
        form = CreateReceipt(user=request.user, instance=model_instance)
    context = {
        "form": form,
    }
    return render(request, "receipts/edit.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category_list}
    return render(request, "receipts/category_list.html", context)


@login_required
def edit_category(request, id):
    model_instance = ExpenseCategory.objects.get(id=id)
    if request.method == "POST":
        form = CreateExpenseCategory(request.POST, instance=model_instance.id)
        if form.is_valid():
            model_instance = form.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategory(instance=model_instance)
    context = {
        "form": form,
    }
    return render(request, "receipts/edit_expense_category.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(request, "receipts/account_list.html", context)


@login_required
def edit_account(request, id):
    model_instance = Account.objects.get(id=id)
    if request.method == "POST ":
        form = CreateAccount(request.POST, instance=model_instance.id)
        if form.is_valid():
            model_instance = form.save()
            return redirect("account_list")
    else:
        form = CreateAccount(instance=model_instance)
    context = {
        "form": form,
    }
    return render(request, "receipts/edit_account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateExpenseCategory(request.POST)
        if form.is_valid():
            create = form.save(False)
            create.owner = request.user
            create.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategory()
    context = {"form": form}
    return render(request, "receipts/create_expense_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            create = form.save(False)
            create.owner = request.user
            create.save()
            return redirect("account_list")
    else:
        form = CreateAccount()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
