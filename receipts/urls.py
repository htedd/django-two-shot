from django.urls import path
from receipts.views import (
    home,
    create_receipt,
    category_list,
    account_list,
    create_category,
    create_account,
    edit_receipt,
    edit_category,
    edit_account,
)

urlpatterns = [
    path("", home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("edit/<int:id>/[", edit_receipt, name="edit_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("categories/edit/<int:id>/", edit_category, name="edit_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("accounts/edit/<int:id>/", edit_account, name="edit_account"),
]
